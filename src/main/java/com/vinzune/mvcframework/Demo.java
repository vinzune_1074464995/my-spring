package com.vinzune.mvcframework;

import com.vinzune.mvcframework.serlvet.DispatcherServlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Demo {
    public static void main(String[] args) {
        try {
            Properties configContext = new Properties();
            InputStream is = Demo.class.getClassLoader()
                    .getResourceAsStream("application.properties");
            configContext.load(is);

            String scanPackage = configContext.getProperty("scanPackage");
            System.out.println(scanPackage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
