package com.vinzune.mvcframework.annotation;


import java.lang.annotation.*;

/**
 * 这是自动注入的注解
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface VAutowired {
    String value() default "";

}
