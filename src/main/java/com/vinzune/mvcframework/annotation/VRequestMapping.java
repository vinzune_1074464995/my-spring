package com.vinzune.mvcframework.annotation;

import java.lang.annotation.*;


@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface VRequestMapping {
    String value() default "";
}
