package com.vinzune.mvcframework.annotation;


import java.lang.annotation.*;

/**
 * 这是Controller注解
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface VService {
    String value() default "";
}
