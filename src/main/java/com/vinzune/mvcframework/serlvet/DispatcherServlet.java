package com.vinzune.mvcframework.serlvet;

import com.vinzune.mvcframework.annotation.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DispatcherServlet  extends HttpServlet {

    private ConcurrentHashMap<String,Object> mapping = new ConcurrentHashMap<>();

    private List<String> classNames = new ArrayList<>();

    private List<Handler> handlerMapping = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            dpDispatch(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 请求分发的方法
     * @param req
     * @param resp
     */
    private void dpDispatch(HttpServletRequest req, HttpServletResponse resp) throws Exception{
        Handler handler=getHandler(req);
        if(handler==null){
            resp.getWriter().write("404 啥也找不到");
            return;
        }
        //获取方法的形参列表
        Class<?>[] paramTypes = handler.getParameterTypes();
        Object[] paramValues = new Object[paramTypes.length];
        Map<String, String[]> parameterMap = req.getParameterMap();
        for(Map.Entry<String,String[]> set:parameterMap.entrySet()){
            String value = Arrays.toString(set.getValue()).replaceAll("\\[|\\]","").replaceAll("\\s",",");
            if(!handler.parameterIndexMapping.containsKey(set.getKey()))
                continue;
            Integer index = handler.parameterIndexMapping.get(set.getKey());
            paramValues[index] = convert(paramTypes[index],value);
        }
        if(handler.parameterIndexMapping.containsKey(HttpServletRequest.class.getName())){
            Integer index = handler.parameterIndexMapping.get(HttpServletRequest.class.getName());
            paramValues[index]=req;
        }
        if(handler.parameterIndexMapping.containsKey(HttpServletResponse.class.getName())){
            Integer index = handler.parameterIndexMapping.get(HttpServletResponse.class.getName());
            paramValues[index]=resp;
        }
        Object returnValue = handler.method.invoke(handler.controller, paramValues);
        if(returnValue == null || returnValue instanceof Void) return;
        resp.getWriter().write(returnValue.toString());
    }

    private Object convert(Class<?> paramType, String value) {
        if(Integer.class==paramType)
            return Integer.valueOf(value);
        return value;
    }

    private Handler getHandler(HttpServletRequest req) {
        try {
            if(handlerMapping.isEmpty()) return null ;
            String url=req.getRequestURI();
            String contextPath=req.getContextPath();
            url=url.replace(contextPath,"").replaceAll("/+","/");
            for (Handler handler : handlerMapping){
                Matcher matcher = handler.pattern.matcher(url);
                if(!matcher.matches()) continue;
                return handler;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        //从配置文件中获取需要扫描的包信息
        String scanPackage = getProperties(config);
        //先扫描包下所有的类文件
        doScan(scanPackage);
        //遍历所有的类 将类名和类实体对象对应的放进mapping中
        doInstance();
        //进行依赖注入
        doAutowired();
        //requestMapping处理
        initHandleMapping();
        System.out.println("都完成了");
    }


    private String getProperties(ServletConfig config) {
        String scanPackage="";
        try {
            Properties configContext = new Properties();
            InputStream is = this.getClass().getClassLoader()
                    .getResourceAsStream(config.getInitParameter("contextConfigLocation"));
            configContext.load(is);

            scanPackage = configContext.getProperty("scanPackage");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scanPackage;
    }




    /**
     * 处理Controller中的RequestMapping
     *
     */
    private void initHandleMapping() {
        if(mapping.isEmpty()) return;
        for (Map.Entry<String ,Object> set: mapping.entrySet()) {
            Class<?> aClass = set.getValue().getClass();
            if(!aClass.isAnnotationPresent(VController.class)) continue;
            //保存在Controller类上的基础URL
            String baseUrl="";
            if(aClass.isAnnotationPresent(VRequestMapping.class)){
                VRequestMapping annotation = aClass.getAnnotation(VRequestMapping.class);
                baseUrl =annotation.value();
            }

            for (Method m : aClass.getMethods()) {
                if(!m.isAnnotationPresent(VRequestMapping.class)) continue;
                VRequestMapping annotation = m.getAnnotation(VRequestMapping.class);
//                String url = ("/"+baseUrl+"/"+annotation.value()).replaceAll("/+","/");
//                handlerMapping.put(url,m);
                String regex = ("/"+baseUrl+"/"+annotation.value()).replaceAll("/+","/");
                Pattern pattern = Pattern.compile(regex);
                handlerMapping.add(new Handler(set.getValue(),m,pattern));
                System.out.println("Mapped :"+regex+ " : "+m);
            }
        }
    }

    /**
     *进行依赖注入
     */
    private void doAutowired() {
        if(mapping.isEmpty()) return;
        for (Map.Entry<String ,Object> set: mapping.entrySet()) {

            Field[] declaredFields = set.getValue().getClass().getDeclaredFields();
            for (Field field : declaredFields){
                if(!field.isAnnotationPresent(VAutowired.class)) continue;
                VAutowired annotation = field.getAnnotation(VAutowired.class);

                //如果没有显式的声明beanName
                String beanName = annotation.value().trim();
                if("".equals(beanName)){
                   beanName = field.getType().getName();
                }
                //访问权限打开
                field.setAccessible(true);

                try {
                    //使用反射动态给字段赋值
                    field.set(set.getValue(),mapping.get(beanName));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void doInstance() {
       if(classNames.isEmpty()) return;
        try {
            for (String className: classNames) {
                Class<?> aClass = Class.forName(className);
                //如果是Controller
                if(aClass.isAnnotationPresent(VController.class)){
                    Object o = aClass.getDeclaredConstructor().newInstance();
                    String beanName = toLowerFirstCase(className);
                    mapping.put(beanName,o);
                }
                else  if(aClass.isAnnotationPresent(VService.class)){
                    //自定义的ServiceName
                    VService annotation = aClass.getAnnotation(VService.class);
                    String serviceName = annotation.value();
                    Object o = aClass.getDeclaredConstructor().newInstance();
                    //如果serviceName 为空的话就设置默认的serviceName
                    if(serviceName.trim() .equals(""))
                        serviceName = toLowerFirstCase(className);
                    mapping.put(serviceName,o);
                    for(Class<?> clazz :  aClass.getInterfaces()){
                        if(mapping.containsKey(clazz.getName()))
                            throw new Exception("The "+clazz.getName()+"已经存在");
                        mapping.put(clazz.getName(),o);
                    }
                }
                else {
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    private String toLowerFirstCase(String className) {
        char[] chars = className.toCharArray();
        chars[0]+=32;
        return String.valueOf(chars);
    }


    /**
     * 扫描所有的类
     * @param scanPackage
     */
    private void doScan(String scanPackage) {
        URL url = this.getClass().getClassLoader()
                .getResource("/" + scanPackage.replaceAll("\\.", "/"));
        File file = new File(url.getFile());
        for(File f : file.listFiles()){
            if(f.isDirectory()) {
                doScan(scanPackage+"."+f.getName()); //还是会将.替换为 /
            }
            else {
                if(!f.getName().endsWith(".class")) {continue;}  //不是Class文件就跳过
                String clazzName= scanPackage+"." +f.getName().replace(".class",""); //com.vinzune.xxx
                classNames.add(clazzName);
            }
        }

    }


    /**
     * 记录Controller中Method和RequestMapping对应的关系
     */
    private class Handler{
        protected Object controller;
        protected Method method;
        protected Pattern pattern;
        protected Map<String ,Integer> parameterIndexMapping;

        public Handler(Object controller, Method method, Pattern pattern) {
            this.controller = controller;
            this.method = method;
            this.pattern = pattern;
            parameterIndexMapping = new HashMap<>();
            putParameterIndexMapping(method);
        }

        private void putParameterIndexMapping(Method method){
            //提取方法中加了注解的参数
            Annotation[][] pa = method.getParameterAnnotations();
            for(int i=0;i< pa.length; i++){
                for(Annotation a : pa[i]){
                    if( a instanceof VRequestParam){
                        String paramName = ((VRequestParam) a).value();
                        if(!"".equals(paramName))
                            parameterIndexMapping.put(paramName,i);
                    }
                }
            }
            //提取方法中的request和response参数
            Class<?>[] parameterTypes = method.getParameterTypes();
            for (int i=0; i<parameterTypes.length; i++){
                Class<?> parameterType = parameterTypes[i];
                if(parameterType==HttpServletRequest.class || parameterType==HttpServletResponse.class)
                    parameterIndexMapping.put(parameterType.getName(),i);
            }
        }

        public Class<?>[] getParameterTypes() {
            return  method.getParameterTypes();
        }
    }
}
