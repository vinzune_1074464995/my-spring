package com.vinzune.mvcframework.service.impl;

import com.vinzune.mvcframework.annotation.VService;
import com.vinzune.mvcframework.service.DemoService;

@VService
public class DemoServiceImpl  implements DemoService {
    @Override
    public String getName(String name) {
        return "My name is "+ name;
    }
}
