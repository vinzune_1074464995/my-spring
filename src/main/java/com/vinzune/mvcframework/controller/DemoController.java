package com.vinzune.mvcframework.controller;

import com.vinzune.mvcframework.annotation.VAutowired;
import com.vinzune.mvcframework.annotation.VController;
import com.vinzune.mvcframework.annotation.VRequestMapping;
import com.vinzune.mvcframework.annotation.VRequestParam;
import com.vinzune.mvcframework.service.DemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@VController
public class DemoController {
    @VAutowired
    private DemoService demoServiceImpl;

    @VRequestMapping("/hello")
    public String hello(HttpServletRequest request, HttpServletResponse response,
                        @VRequestParam("name") String name){
        return demoServiceImpl.getName(name);
    }

}
